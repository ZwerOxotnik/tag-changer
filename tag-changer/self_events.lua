return {
	-- Called when a player successfully changed tag.
	--	Contains:
	--		player_index :: uint: The index of the player.
	on_new_tag = script.generate_event_name(),
}