--[[
Copyright (C) 2019-2020 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the EUPL, Version 1.2 only (the "LICENCE");
Author: ZwerOxotnik

-- You can write and receive any information on the links below.
-- Source: https://gitlab.com/ZwerOxotnik/tag-changer
-- Mod portal: https://mods.factorio.com/mod/tag-changer
]]--

local mod = {}
mod.self_events = require("tag-changer/self_events")
-- TODO: add handling mute, unmute player events

local function change_player_tag(player)
	local new_tag = player.mod_settings["newTag"].value
	if string.len(new_tag) > 200 then player.print({"tag-changer.too_much_long_tag"}) return end
    if new_tag == player.tag then return end
	if tonumber(new_tag) then player.print({"tag-changer.tag"}) return end

	player.tag = new_tag
	script.raise_event(mod.self_events.on_new_tag, {player_index = player.index})
end

local function on_player_joined_game(event)
	-- Validation of data
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end

	change_player_tag(player)
end

local function on_runtime_mod_setting_changed(event)
	-- Validation of data
	if event.setting_type ~= "runtime-per-user" then return end
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end

	if event.setting == "newTag" then
		change_player_tag(player)
	end
end

local function tag_command(cmd)
	if cmd.player_index == nil then return end

	local player = game.players[cmd.player_index]
	if cmd.parameter == nil then
		player.tag = ""
		script.raise_event(mod.self_events.on_new_tag, {player_index = player.index})
	else
		if string.len(cmd.parameter) > 200 then player.print({"tag-changer.too_much_long_tag"}) return end
		if cmd.parameter == player.tag then return end
		player.tag = cmd.parameter
		script.raise_event(mod.self_events.on_new_tag, {player_index = player.index})
	end
end

mod.add_commands = function()
	commands.add_command("tag", {"tag-changer.tag"}, tag_command)
end

mod.add_remote_interface = function()
	remote.remove_interface('tag-changer')
	remote.add_interface('tag-changer',
	{
		get_event_name = function(name)
			return mod.self_events[name]
		end
	})
end

mod.events = {
	[defines.events.on_player_joined_game] = on_player_joined_game,
	[defines.events.on_runtime_mod_setting_changed] = on_runtime_mod_setting_changed
}

return mod
