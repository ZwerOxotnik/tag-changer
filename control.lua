local event_handler = require("event_handler")
local modules = {}
modules.name_changer = require("tag-changer/control")

event_handler.add_libraries(modules)
