data:extend({
	{
		type = "string-setting",
		name = "newTag",
		setting_type = "runtime-per-user",
        default_value = "",
		allow_blank = true
	}
})
